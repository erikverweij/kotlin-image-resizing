package com.example

import io.ktor.http.Parameters
import java.io.File
import java.util.*

private fun queryOptionsParser(parameters: Parameters): (String) -> Int? {

    fun parseParam(key: String): Int? {
        val value = parameters[key] ?: return null

        return try {
            Integer.parseInt(value)
        } catch (e: Throwable) {
            null
        }

    }

    return ::parseParam
}

data class ImageOptions(val uuid: UUID, val width: Int?, val height: Int?, val quality: Int? = 80) {

    private fun isSet(): Boolean {
        return height != null || width != null
    }

    private fun getFileNameByOptions(): String {
        return arrayOf(
            if (width is Int) "w$width" else null,
            if (height is Int) "h$height" else null,
            if (quality is Int) "q$quality" else null
        ).filterNotNull().joinToString("_") + ".jpg"
    }

    fun getSourceFile(): File {
        return File("${getImageFolderById(uuid)}/$IMAGE_SOURCE_NAME");
    }

    fun getFile(): File {
        val basePath = getImageFolderById(uuid)

        if (isSet()) {
            return File("$basePath/${getFileNameByOptions()}")
        }

        return getSourceFile()

    }

    companion object {
        fun fromQueryParameters(uuid: UUID, parameters: Parameters): ImageOptions {
            val parseKey = queryOptionsParser(parameters)

            return ImageOptions(uuid, parseKey("w"), parseKey("h"), parseKey("q"))
        }
    }
}