package com.example

import io.ktor.application.call
import io.ktor.client.call.receive
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.MultiPartData
import io.ktor.http.content.PartData
import io.ktor.http.content.readAllParts
import io.ktor.http.content.streamProvider
import io.ktor.http.contentType
import io.ktor.request.receive
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.response.respondFile
import io.ktor.routing.Routing
import io.ktor.routing.*
import java.awt.image.BufferedImage
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.util.*
import javax.imageio.ImageIO

/**
 * UTIL
 */

val ASSET_DIR = File("./assets");
const val IMAGE_SOURCE_NAME = "source.jpg";

fun getImageFolderById(uuid: UUID): File = File("${ASSET_DIR}/$uuid")

private suspend fun getFileFromMultiPart(multipart: MultiPartData, fieldName: String = "file"): PartData.FileItem {
    val parts = multipart.readAllParts();

    val filePart = parts.find { it.name == fieldName }

    if (filePart == null) {
        throw IOException("Please use the 'file' field to upload a binary image")
    } else if (filePart !is PartData.FileItem) {
        throw IOException("Field 'file' must be of type binary")
    }

    return filePart
}

fun createAssetFolderIfNecessary() {
    if (!ASSET_DIR.exists()) {
        ASSET_DIR.mkdir()
    }
}

fun writeImageToFile(image: BufferedImage, location: File) {
    if (!ImageIO.write(image, "jpg", location)) {
        throw IOException("Unable to write image to file")
    }
}

fun createFullQualityImageFromStream(stream: InputStream): UUID {
    // create image buffer
    val fullQualityImage = createFullQualityJpgBuffer(stream)

    // give image a unique name
    val id = UUID.randomUUID()

    // create new folder based on id
    val imageFolder = getImageFolderById(id)
    imageFolder.mkdir()

    // write image to folder
    writeImageToFile(fullQualityImage, File("$imageFolder/$IMAGE_SOURCE_NAME"))

    return id
}

data class Identity (val id: UUID)

/**
 * ROUTES
 */

fun Routing.root() {
    post("/upload") {

        // create a place to store the images if not exist
        createAssetFolderIfNecessary()

        // get the filePart
        val filePart = try {
            getFileFromMultiPart(call.receiveMultipart())
        } catch (e: IOException) {
            call.respond(HttpStatusCode.BadRequest, e.message ?: "")
            return@post
        }

        try {
            val id = createFullQualityImageFromStream(filePart.streamProvider())

            call.respond(HttpStatusCode.Created, Identity(id))

        } catch (e: Throwable) {
            call.respond(HttpStatusCode.InternalServerError, e?.message ?: "")
        }

    }

    post("/url") {

        val body = call.receive<UrlRequest>();

        if (body.url == null) {
            call.respond(HttpStatusCode.BadRequest, "Please provide an 'url' in the body")
            return@post
        }

        val response: HttpResponse = client.get(body.url)

        if (response.status != HttpStatusCode.OK) {
            call.respond(HttpStatusCode.BadRequest, "Please provide an valid 'url' in the body")
            return@post
        }

        val contentType = response.contentType()

        if (contentType == null) {
            call.respond(HttpStatusCode.BadRequest, "The url has an invalid content-type")
            return@post
        }

        if (!contentType.match(ContentType.Image.Any)) {
            call.respond(HttpStatusCode.BadRequest, "The url must point to an image")
            return@post
        }

        try {
            val id = createFullQualityImageFromStream(response.receive())

            call.respond(HttpStatusCode.Created, Identity(id))

        } catch (e: Throwable) {
            call.respond(HttpStatusCode.InternalServerError, e?.message ?: "")
        }
    }

    get("/images/{id}") {
        // get UUID from parameters
        val uuid = call.parameters["id"];

        val imageId = try {
            UUID.fromString(uuid)
        } catch (e: Throwable) {
            call.respond(HttpStatusCode.BadRequest, "Please provide a valid image-id")
            return@get
        }

        try {
            // construct options object from query parameters
            val options = ImageOptions.fromQueryParameters(imageId, call.request.queryParameters)

            // get pointer to file
            val file = options.getFile();

            if (file.exists()) {
                // seems the image is already there -> serve it!
                call.respondFile(file)
            } else {
                // seems like we have to create the image first
                val resizedImage = resizeImage(options).get()
                writeImageToFile(resizedImage, file)
                call.respondFile(file)
            }

        } catch (e: Throwable) {
            call.respond(HttpStatusCode.InternalServerError, e?.message ?: "")
        }

    }

    get("/images") {
        // get list of all folder-names inside the asset folder
        call.respond(ASSET_DIR.listFiles().map { it.name })
    }
}