package com.example

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.InputStream
import javax.imageio.ImageIO
import org.imgscalr.AsyncScalr
import java.util.concurrent.Future

fun createFullQualityJpgBuffer(input: InputStream): BufferedImage {
    val sourceImage = ImageIO.read(input);

    val result = BufferedImage(
        sourceImage.width,
        sourceImage.height,
        BufferedImage.TYPE_INT_RGB
    )

    result.createGraphics().drawImage(sourceImage, 0, 0, Color.WHITE, null)

    return result
}

fun resizeImage(options: ImageOptions): Future<BufferedImage> {
    val sourceImage = ImageIO.read(options.getSourceFile());

    // TODO: only width option works right now -> add height and quality options later...
    return AsyncScalr.resize(sourceImage, options.width ?: 200)

}